/*
 * Copyright (C) Ludvig Hummel, Richard Ferreira
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR THE COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written authorization.
 */


/* Read the font family and style from font files and rename them accordingly
 *
 * Requirements: freetype (>= 2.4.6)
 *
 * Mostly working; may still have some file-eating bugs. Use at your own risk.
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <getopt.h>
#include <limits.h>
#include <stdlib.h>
#include <ft2build.h>
#include <locale.h>
#include <ctype.h>
#include <ftw.h>
#include "error.h"
#include <time.h>
#include FT_FREETYPE_H
#include FT_XFREE86_H

#define FRBUFSIZ 7168

struct option tfoptions[] = {
	{"cleanup", required_argument, 0, 'c'},
	{"recursive", no_argument, 0, 'r'},
	{"dry-run", no_argument, 0, 'd'},
	{"log-file", optional_argument, 0, 'l'},
	{"verbose", no_argument, 0, 'v'},
	{"help", no_argument, 0, 'h'},
	{"version", no_argument, 0, 'V'},
	{"no-mangling", no_argument, 0, 'm'},
	{"organize", no_argument, 0, 'o'},
	{"scan-all", no_argument, 0, 'a'},
	{ 0, 0, 0, 0 }
};

typedef enum {
	LOG_WARN,
	LOG_ERROR,
	LOG_INFO
} log_tag_t;

struct ffrdata {
	FT_Face ftface;
	char *newname;
	char *pfm_old;
	char *pfm_new;
	char *afm_old;
	char *afm_new;
};

#define FFR_ORGANIZE		(1 << 0)
#define FFR_NO_MANGLING		(1 << 1)
#define FFR_RECURSIVE		(1 << 2)
#define FFR_DRY_RUN		(1 << 3)
#define FFR_VERBOSE		(1 << 4)
#define FFR_SCAN_ALL		(1 << 5)
#define FFR_CLEAN_XFM		(1 << 6)
#define FFR_CLEAN_UNREADABLE	(1 << 7)
#define FFR_CLEAN_UNKNOWN	(1 << 8)
#define FFR_CLEAN_ALL		(1 << 9)
#define is_set(flag)		(flag & ff_flags)

int ff_flags = 0;

FT_Library ftlib;
FILE *loghandler = NULL;
char program_name[FRBUFSIZ] = "";

void error(int exitcode, int errnum, const char *fmt, ...);
void ffr_end(void);
void safe_unlink(const char *file, const char *reason, ...);
void ffrdata_free(struct ffrdata **data);
FT_Face get_ftface(const char *filename);

#define FORBIDDEN_CHARS " *?/\\:;(){}[]&!=+%$#@'\"~"
#define DEFAULT_LOGFILE "./ffrename.log"

int is_forbidden(const char *blacklist, int c)
{
	for (; *blacklist; blacklist++)
		if (c == *blacklist)
			return 0;
	return -1;
}

char *strtidy(char *s, int (*newchar)(int oldchar))
{
	char *start;
	int c;

	for (start = s; *s; s++) {
		if ((c = newchar(*s)) < 0)
			continue;
		if (c)
			*s = c;
		if (s > start && (!c || *(s - 1) == c)) {
			memmove(s, s + 1, strlen(s - 2));
			s--;
		}
	}
	return start;
}

void fflog(const char *caller, log_tag_t level, const char *message, ...)
{
	va_list argp;
	time_t rt;
	struct tm *lt;
	char buf[FRBUFSIZ];
	char timestamp[FRBUFSIZ];
	char tag[FRBUFSIZ];

	switch (level) {
	case LOG_WARN:
		strncpy(tag, "warning", sizeof(tag));
		break;
	case LOG_ERROR:
		strncpy(tag, "error", sizeof(tag));
		break;
	case LOG_INFO:
		strncpy(tag, "info", sizeof(tag));
		break;
	default:
		break;
	}
	rt = time(NULL);
	lt = localtime(&rt);
	strftime(timestamp, sizeof(timestamp), "%F %T", lt);
	va_start(argp, message);
	vsnprintf(buf, sizeof(buf), message, argp);
	va_end(argp);
	if (loghandler)
		fprintf(loghandler, "[%s] (%s) <%s> %s\n", timestamp, caller, tag, buf);
	if (ff_flags & FFR_VERBOSE)
		fprintf(stderr, "[%s] (%s) <%s> %s\n", timestamp, caller, tag, buf);
}

void xfmcleanup(void)
{
	struct dirent **de;
	int i;
	char *ext;
	char buf[FRBUFSIZ] = "";

	if ((i = scandir(".", &de, NULL, alphasort)) == -1) {
		fflog(__func__, LOG_ERROR, "scandir: %s", strerror(errno));
		return;
	}
	while (i--) {
		if (!strcmp(de[i]->d_name, ".") || !strcmp(de[i]->d_name, ".."))
			continue;
		if (!(ext = strrchr(de[i]->d_name, '.')))
			continue;
		if (strcasecmp(ext, ".pfm") && strcasecmp(ext, ".afm"))
			continue;
		strncpy(buf, de[i]->d_name, strlen(de[i]->d_name - strlen(ext) - 1));
		strncat(buf, ".pfb", sizeof(buf) - 1);
		safe_unlink(buf, "%s: stale %s file (removed as requested)", __func__, ext);
		free(de[i]);
	}
	free(de);
}

void safe_unlink(const char *file, const char *reason, ...)
{
	va_list argp;
	char buf[FRBUFSIZ];

	va_start(argp, reason);
	vsnprintf(buf, sizeof(buf), reason, argp);
	va_end(argp);
	if (!is_set(FFR_DRY_RUN) && unlink(file) != 0) {
		if (errno == ENOENT)
			return;
		fflog(__func__, LOG_ERROR, "%s: unlink: %s", file, strerror(errno));
		return;
	}
	fflog(__func__, LOG_INFO, "%s: %s", file, buf);
}

char *get_extension(const FT_Face ftface)
{
	const char *fformat = FT_Get_X11_Font_Format(ftface);
	int i;
	struct {
		char *ffstring;
		char *ext;
	} fntext[] = {
		{"TrueType", ".ttf"},
		{"Type 1", ".pfb"},
		{"Type 42", ".pfb"},
		{"CID Type 1", ".pfb"},
		{"CFF", ".otf"},
		{"BDF", ".bdf"},
		{"PCF", ".pcf"},
		{"Windows FNT", ".fnt"},
		{NULL, NULL}
	};
	
	for (i = 0; fformat && fntext[i].ffstring; i++) {
		if (strcasecmp(fformat, fntext[i].ffstring) == 0)
			return fntext[i].ext;
	}
	return NULL;
}

void safe_rename(const char *oldname, const char *newname)
{
	if (!oldname)
		return;
	if (!strcmp(oldname, newname))
		return;
	if (is_set(FFR_DRY_RUN)) {
		fflog(__func__, LOG_INFO, "%s (%s)", newname, oldname);
		return;
	}
	if (rename(oldname, newname) < 0) {
		fflog(__func__, LOG_ERROR, "%s: rename: %s", oldname, strerror(errno));
		return;
	}
	fflog(__func__, LOG_INFO, "%s (%s)", newname, oldname);
}

char *get_subdir(const char *filename)
{
	static char buf[3];

	buf[0] = isalpha(*filename) ? tolower(*filename) : '#';
	buf[1] = '/';
	buf[2] = 0;
	return buf;
}

char *extsub(const char *filename, const char *ext)
{
	char *dot = NULL;
	long namelen = strlen(filename) + 5;
	char *ret = malloc(namelen);

	memset(ret, 0, namelen - 1);
	dot = strrchr(filename, '.');
	memcpy(ret, filename, dot ? dot - filename : namelen - 5);
	strncat(ret, ext, namelen - strlen(ret) - 1);
	return ret;
}

int safe_mkdir(const char *dir, mode_t mode)
{
	struct stat st;

	if (stat(dir, &st) < 0 && errno != ENOENT) {
		fflog(__func__, LOG_ERROR, "stat: %s: %s", dir, strerror(errno));
		return -1;
	} else {
		if (!S_ISDIR(st.st_mode)) {
			fflog(__func__, LOG_ERROR, "\"%s\" exists but is not a directory", dir);
			return -1;
		}
		return 0;
	}
	
	if (mkdir(dir, mode) < 0) {
		fflog(__func__, LOG_ERROR, "mkdir: %s: %s", dir, strerror(errno));
		return -1;
	}
	fflog(__func__, LOG_INFO, "created directory \"%s\"", dir);
	return 0;
}

int newchar(int c)
{
	return (!isascii(c) || is_forbidden(FORBIDDEN_CHARS, c) == 0) ? 0 : -1;
}

struct ffrdata *make_newname(const char *filename)
{
	int i;
	struct ffrdata *data;
	char buf[FRBUFSIZ] = "";
	char *ext;
	char *font_style;
	char *subdir = NULL;
	
	if (!(data = malloc(sizeof(*data)))) {
		fflog(__func__, LOG_ERROR, "malloc: %s", strerror(errno));
		return NULL;
	}
	memset(data, 0, sizeof(*data));
	if (!(data->ftface = get_ftface(filename))) {
		ffrdata_free(&data);
		return NULL;
	}
	if (!(ext = get_extension(data->ftface))) {
		fflog(__func__, LOG_ERROR, "%s: could not determine file extension", filename);
		ffrdata_free(&data);
		return NULL;
	}
	font_style = data->ftface->style_name ? data->ftface->style_name : "Regular";
	if (!(data->newname = malloc(FRBUFSIZ + 1))) {
		fflog(__func__, LOG_ERROR, "malloc: %s", strerror(errno));
		return NULL;
	}
	if (is_set(FFR_ORGANIZE) && !is_set(FFR_DRY_RUN)) {
		subdir = get_subdir(data->ftface->family_name);
		if (safe_mkdir(subdir, 0755) < 0) {
			fflog(__func__, LOG_ERROR, "files will not be organized");
			subdir = NULL;
		}
	}
	snprintf(data->newname, FRBUFSIZ, "%s-%s%s", data->ftface->family_name, font_style, ext);
	strtidy(data->newname, newchar);

	for (i = 0; access(data->newname, F_OK) == 0 && !is_set(FFR_NO_MANGLING); i++) {
		if (!strcmp(data->newname, filename)) {
			if (subdir)
				break;
			ffrdata_free(&data);
			return NULL;
		}
		snprintf(data->newname, FRBUFSIZ, "%s-%s%03d%s", data->ftface->family_name, font_style, i, ext);
	}
	if (!strcmp(ext, ".pfb")) {
		snprintf(buf, sizeof(buf), "%s%s", subdir ? subdir : "", data->newname);
		data->pfm_new = extsub(buf, ".pfm");
		data->afm_new = extsub(buf, ".afm");
		data->pfm_old = extsub(filename, ".pfm");
		data->afm_old = extsub(filename, ".afm");
	}
	if (subdir) {
		snprintf(buf, sizeof(buf), "%s%s", subdir, data->newname);
		strncpy(data->newname, buf, FRBUFSIZ);
	}
	return data;
}

void ffrdata_free(struct ffrdata **data)
{
	if (!*data)
		return;
	if ((*data)->pfm_old)
		free((*data)->pfm_old);
	if ((*data)->pfm_new)
		free((*data)->pfm_new);
	if ((*data)->afm_old)
		free((*data)->afm_old);
	if ((*data)->afm_new)
		free((*data)->afm_new);
	free((*data)->newname);
	if ((*data)->ftface)
		FT_Done_Face((*data)->ftface);
	free(*data);
	*data = NULL;
}

FT_Face get_ftface(const char *filename)
{
	int e;
	FT_Face ret;
	struct stat st;

	if (stat(filename, &st) < 0) {
		fflog(__func__, LOG_ERROR, "%s: stat: %s", filename, strerror(errno));
		return NULL;
	}
	if (S_ISDIR(st.st_mode)) {
		fflog(__func__, LOG_ERROR, "%s is a directory", filename);
		return NULL;
	}
	if (!S_ISREG(st.st_mode)) {
		fflog(__func__, LOG_ERROR, "%s is not a regular file", filename);
		return NULL;
	}

	e = errno = 0;
	if ((e = FT_New_Face(ftlib, filename, 0, &ret))) {
		switch (e) {
		case FT_Err_Unknown_File_Format:
			if (is_set(FFR_CLEAN_UNKNOWN)) {
				safe_unlink(filename, "unknown file format (removed as requested)");
				break;
			}
			fflog(__func__, LOG_ERROR, "%s: unknown file format", filename);
			break;
		default:
			if (is_set(FFR_CLEAN_UNREADABLE)) {
				safe_unlink(filename, "file was unreadable (removed as requested)");
				break;
			}
			fflog(__func__, LOG_ERROR, "%s: could not read file: %s", filename, errno ? strerror(errno) : "<errno == 0>");
		}
		return NULL;
	}
	return ret;
}

void ffrename(const char *filename)
{
	struct ffrdata *data;

	if (!(data = make_newname(filename))) {
		fflog(__func__, LOG_INFO, "%s: nothing to do", filename);
		return;
	}
	if (is_set(FFR_NO_MANGLING)) {
		safe_unlink(filename, "duplicate (removed as requested)");
		ffrdata_free(&data);
		return;
	}
	safe_rename(filename, data->newname);
	if (data->afm_old)
		safe_rename(data->afm_old, data->afm_new);
	if (data->pfm_old)
		safe_rename(data->pfm_old, data->pfm_new);
	ffrdata_free(&data);
	return;
}

void usage(void)
{
	fprintf(stderr, "ffrename [options] <folder1> <folder2>...\n\n"
		"-r, --recursive        Recursive mode (descend into subdirectories)\n"
		"-d, --dry-run          Make no changes - only report what would be done\n"
		"-l, --log-file=name    Write error messages to a file. If no file is specified,\n"
		"                       the default one, ./ffrename.log, is used.\n"
		"-v, --verbose          output lots of debugging messages to stderr\n"
		"-m, --no-mangling      Remove duplicates instead of renaming them\n"
		"-o, --organize         Organize fonts by name\n"
		"-c, --cleanup=type     Removal of stale/corrupted files (see below)\n\n"
		"Valid cleanup types are:\n\n"
		"- xfm          remove stale pfm/afm files (i.e., files without a matching .pfb)\n"
		"- unreadable   remove unreadable files\n"
		"- unknown      remove unknown files\n"
		"- all          do all of the above\n"
	);
	exit(EXIT_FAILURE);
}

void version(void)
{
	fprintf(stderr, "%s %s\n", PACKAGE, VERSION);
	exit(EXIT_FAILURE);
}

int tfftw(const char *fpath, const struct stat *st, int typeflag, struct FTW *ftwbuf)
{
	(void)st;
	char buf[FRBUFSIZ];
	
	if (typeflag == FTW_F) {
		ffrename(fpath + ftwbuf->base);
		return 0;
	}
	if (ftwbuf->level)
		return 0;
	getcwd(buf, sizeof(buf));
	if (is_set(FFR_CLEAN_XFM))
		xfmcleanup();
	return 0;
}

int duplicate(const char *compare, char *const argv[], int argc)
{
	int i;

	for (i = 0; i < argc; i++) {
		if (compare == argv[i])
			break;
		if (strcmp(compare, argv[i]) == 0)
			return 1;
	}
	return 0;
}

int main(int argc, char **argv)
{
	int c, i, f;
	struct dirent **de;
	char cwd[FRBUFSIZ];

	atexit(ffr_end);
	setlocale(LC_ALL, NULL);
	strncpy(program_name, "ffrename", sizeof(program_name) - 1);
	while ((c = getopt_long(argc, argv, "rdl::hVvc:moa", tfoptions, 0)) != -1) {
		switch (c) {
		case 'o':
			ff_flags |= FFR_ORGANIZE;
			break;
		case 'm':
			ff_flags |= FFR_NO_MANGLING;
			break;
		case 'r':
			ff_flags |= FFR_RECURSIVE;
			break;
		case 'd':
			ff_flags |= FFR_DRY_RUN;
			break;
		case 'l':
			if (!(loghandler = fopen(optarg ? optarg : "./ffrename.log", "w+")))
				error(1, errno, "could not open log file");
			break;
		case 'h':
			usage();
			break;
		case 'V':
			version();
			break;
		case 'v':
			ff_flags |= FFR_VERBOSE;
			break;
		case 'c':
			if (!strcmp(optarg, "xfm"))
				ff_flags |= FFR_CLEAN_XFM;
			else if (!strcmp(optarg, "unreadable"))
				ff_flags |= FFR_CLEAN_UNREADABLE;
			else if (!strcmp(optarg, "unknown"))
				ff_flags |= FFR_CLEAN_UNKNOWN;
			else if (!strcmp(optarg, "all"))
				ff_flags |= FFR_CLEAN_ALL;
			else
				error(EXIT_FAILURE, errno, "invalid parameter: %s", optarg);
			break;
		default:
			usage();
			exit(EXIT_FAILURE);
		}
	}
	if (argc <= optind) {
		fprintf(stderr, "no directories given\n");
		exit(EXIT_FAILURE);
	}
	if (FT_Init_FreeType(&ftlib)) {
		fflog(__func__, LOG_ERROR, "could not initialize freetype");
		exit(EXIT_FAILURE);
	}
	getcwd(cwd, sizeof(cwd));
	for (i = optind; i < argc; i++) {
		if (duplicate(argv[i], argv, argc))
			continue;
		if (is_set(FFR_RECURSIVE)) {
			if (nftw(argv[i], tfftw, 50, FTW_CHDIR|FTW_PHYS|FTW_DEPTH) == -1) {
				fflog(__func__, LOG_ERROR, "%s: nftw: %s", argv[i], strerror(errno));
				continue;
			}
			printf("ffrename: '%s' done\n", argv[i]);
			continue;
		}
		if ((f = scandir(argv[i], &de, 0, alphasort)) == -1) {
			error(0, errno, "scandir: %s", argv[i]);
			continue;
		}
		if (chdir(argv[i]) < 0) {
			error(0, errno, "chdir: %s", argv[i]);
			continue;
		}
		fflog(__func__, LOG_INFO, "processing %s", argv[i]);
		for (c = 0; c < f; c++) {
			if (!strcmp(de[c]->d_name, ".") || !strcmp(de[c]->d_name, "..")) {
				free(de[c]);
				continue;
			}
			ffrename(de[c]->d_name);
			free(de[c]);
		}
		free(de);
		if (is_set(FFR_CLEAN_XFM))
			xfmcleanup();
		if (chdir(cwd) < 0)
			error(EXIT_FAILURE, errno, "chdir");
		fflog(__func__, LOG_INFO, "'%s' done", argv[i]);
		printf("ffrename: '%s' done\n", argv[i]);
	}
	FT_Done_FreeType(ftlib);
	return 0;
}

void ffr_end(void)
{
	if (loghandler)
		fclose(loghandler);
}

void error(int exitcode, int errnum, const char *fmt, ...)
{
	va_list argp;

	fflush(stderr);
	va_start(argp, fmt);
	if (*program_name)
		fprintf(stderr, "%s: ", program_name);
	vfprintf(stderr, fmt, argp);
	va_end(argp);
	if (errnum > 0)
		fprintf(stderr, ": %s", strerror(errnum));
	putc('\n', stderr);
	if (exitcode > 0)
		exit(exitcode);
}
