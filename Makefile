PACKAGE = ffrename
VERSION = 0.1
SOURCES = ffrename.c
LIBS = 
DIST = $(SOURCES) Makefile
OBJECTS = $(SOURCES:.c=.o) $(LIBS:.c=.o)
EXECUTABLE = ffrename
LDFLAGS += -lfreetype -Wl,--as-needed
CC = gcc
CFLAGS += -std=c99 -Wall -Wextra -pedantic -g3
CPPFLAGS += -D_GNU_SOURCE -I/usr/include/freetype2 -DAUTHOR='"$(AUTHOR)"' -DVERSION='"$(VERSION)"' -DPACKAGE='"$(PACKAGE)"'

all: $(EXECUTABLE) $(OBJECTS)

$(EXECUTABLE): $(OBJECTS)
	$(CC) -o $(EXECUTABLE) $(OBJECTS) $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -c -o $@ $<

dist:
	test -d $(PACKAGE)-$(VERSION) && rm -rf $(PACKAGE)-$(VERSION) || true
	mkdir -p $(PACKAGE)-$(VERSION)/$(STRLIB_SUBDIR)
	cp $(DIST) $(PACKAGE)-$(VERSION)
	cp $(STRLIB_SUBDIR)/{*.c,*.h,LICENSE} $(PACKAGE)-$(VERSION)/$(STRLIB_SUBDIR)
	tar -jcf $(PACKAGE)-$(VERSION).tar.bz2 $(PACKAGE)-$(VERSION)
	rm -rf $(PACKAGE)-$(VERSION)

clean:
	rm -f *.o $(EXECUTABLE)


